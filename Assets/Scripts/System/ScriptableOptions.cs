using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(fileName = "Options", menuName = "Scriptables/Options", order = 3)]
public class ScriptableOptions : ScriptableObject
{
    public float masterLevel;
    public float uiLevel;
    public float actionLevel;
    public float infoLevel;
    public float brightness;
    public int currentResolution;
    public Resolution[] resolutions;
    public ScriptablePalettes brightnessColors;
    public int[] characterSpeeds;


    public void CheckRes()
    {
        resolutions = Screen.resolutions.Where(resolution => resolution.refreshRate == 60).ToArray();
    }

    public void LoadMe(SettingsData sd)
    {
        CheckRes();
        if (sd != null)
        {
            masterLevel = sd.masterLevel;
            uiLevel = sd.uiLevel;
            actionLevel = sd.actionLevel;
            actionLevel = sd.actionLevel;
            infoLevel = sd.infoLevel;
            currentResolution = sd.resolution;
            for (int i = 0; i < characterSpeeds.Length; i++)
            {
                characterSpeeds[i] = sd.characterSpeeds[i];
            }
        }
        else
        {
            masterLevel = .5f;
            uiLevel = .5f;
            actionLevel = .5f;
            infoLevel = .5f;
            brightness = 1;
            if (resolutions.Length > 0)
            {
                currentResolution = resolutions.Length / 2;
            }
            else
            {
                currentResolution = 0;
            }
            for (int i = 0; i < characterSpeeds.Length; i++)
            {
                characterSpeeds[i] = 200;
            }
        }
    }
}
