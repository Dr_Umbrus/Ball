using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class SaveSystem
{
    public static void SaveSetting(ScriptableOptions so)
    {
        SettingsData data = new SettingsData(so);



        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/options.sav";
        FileStream stream = new FileStream(path, FileMode.Create);

        formatter.Serialize(stream, data);
        stream.Close();

    }

    public static SettingsData LoadSetting()
    {
        string path = Application.persistentDataPath + "/options.sav";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);


            SettingsData data = formatter.Deserialize(stream) as SettingsData;
            stream.Close();
            return data;

        }
        else
        {
            Debug.Log("Not find settings");
            return null;
        }
    }

}

[System.Serializable]
public class SettingsData
{
    public float masterLevel;
    public float uiLevel;
    public float actionLevel;
    public float infoLevel;
    public float brightness;
    public int resolution;
    public int[] characterSpeeds= new int[4];

    public SettingsData(ScriptableOptions op)
    {
        masterLevel = op.masterLevel;
        uiLevel = op.uiLevel;
        actionLevel = op.actionLevel;
        infoLevel = op.infoLevel;
        brightness = op.brightness;
        resolution = op.currentResolution;
        for (int i = 0; i < characterSpeeds.Length; i++)
        {
            characterSpeeds[i]=op.characterSpeeds[i];
        }
    }
}

