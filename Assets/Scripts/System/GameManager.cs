using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class GameManager : MonoBehaviour
{
    public enum ICurrentMode { PointMatch=0, DeathMatch=1, LastAlive=2}

    public ICurrentMode currentMode=ICurrentMode.PointMatch;
    int currentModeID = 0;

    public List<PlayerInput> players = new List<PlayerInput>();
    public PlayerInputManager pim;
    public InputActionAsset inputs;
    public InputActionMap menuMap, inGameMap;
    public string currentSceneName;
    public ScriptableSceneList scenes;
    public bool[] playerReady = new bool[4];

    public int numberOfPlayers = 0;
    public int targetScorePoint=15;
    public int targetScoreDeath = 5;
    public bool doubleItems=false;

    [Header("Options")]
    public ScriptableOptions options;
    [SerializeField] Image brightness;
    [SerializeField] AudioMixer mixer;

    
    private void Awake()
    {
        pim = FindObjectOfType<PlayerInputManager>();
        menuMap= inputs.FindActionMap("Menu");
        inGameMap = inputs.FindActionMap("InGame");
        options.LoadMe(SaveSystem.LoadSetting());
        ChangeScene(0);
        SetOptions();
    }

    #region Options

    public void SetOptions()
    {
        ChangeBrightness(options.brightness);
        ChangeResolution(options.currentResolution);
        SetMasterVolume(options.masterLevel);
    }
    public void ChangeBrightness(float newBright)
    {
        brightness.color = Color.Lerp(options.brightnessColors.colors[0], options.brightnessColors.colors[1], newBright);
        options.brightness = newBright;
    }

    public void ChangeResolution(int add)
    {
        if (options.resolutions.Length == 0)
        {
            Screen.SetResolution(1920, 1080, false, 60);
        }
        else
        {
            options.currentResolution = add;
            if (options.currentResolution < 0)
            {
                options.currentResolution = options.resolutions.Length - 1;
            }
            else if (options.currentResolution >= options.resolutions.Length)
            {
                options.currentResolution = 0;
            }
            Screen.SetResolution(options.resolutions[options.currentResolution].width, options.resolutions[options.currentResolution].height, false, options.resolutions[options.currentResolution].refreshRate);
        }
    }

    public void SetMasterVolume(float sliderValue)
    {
        mixer.SetFloat("MasterVolume", Mathf.Log10(sliderValue) * 20);
    }

    public void SetUIVolume(float sliderValue)
    {
        mixer.SetFloat("UIVolume", Mathf.Log10(sliderValue) * 20);
    }

    public void SetInfoVolume(float sliderValue)
    {
        mixer.SetFloat("InfoVolume", Mathf.Log10(sliderValue) * 20);
    }

    public void SetGameVolume(float sliderValue)
    {
        mixer.SetFloat("InGameVolume", Mathf.Log10(sliderValue) * 20);
    }

    public void ChangeCharacterSpeed(int character, int sliderValue)
    {
        options.characterSpeeds[character] = sliderValue*10;
    }

    #endregion

    private void OnEnable()
    {
        pim.onPlayerJoined += AddPlayer;
    }

    private void OnDisable()
    {
        pim.onPlayerJoined -= AddPlayer;
    }

    public void AddPlayer(PlayerInput player)
    {
        if (players.Count < 4)
        {
            GetComponent<AudioSource>().Play();
            players.Add(player);
            player.GetComponent<PlayerController>().playerID = players.Count - 1;
            MenuUISystem menu = FindObjectOfType<MenuUISystem>();
            if (menu != null) menu.SetPlayerSelection();
        }       
    }

    public void ChangeMode(int add)
    {
        currentModeID = add;
        if (currentModeID < 0) currentModeID = 2;
        if (currentModeID > 2) currentModeID = 0;

        currentMode = (ICurrentMode)currentModeID;
    }

    public void ChangeTargetScore(int add)
    {
        if (currentMode == ICurrentMode.PointMatch)
        {
            targetScorePoint += add;
            if (targetScorePoint < 5) targetScorePoint = 5;
        }
        else if(currentMode == ICurrentMode.DeathMatch)
        {
            targetScoreDeath += add;
            if (targetScorePoint < 1) targetScorePoint = 1;
        }
   }

    public void ChangeScene(int id)
    {
        SceneManager.LoadSceneAsync(scenes.names[id], LoadSceneMode.Additive);
        
        if(currentSceneName!="") SceneManager.UnloadSceneAsync(currentSceneName);
        currentSceneName = scenes.names[id];
        if (id > 0)
        {
            SwitchMode(false);
        }
    }

    public void SwitchMode(bool backToMenu)
    {
        for (int i = 0; i < players.Count; i++)
        {
            if (backToMenu)
            {
                players[i].SwitchCurrentActionMap(menuMap.name);
            }
            else
            {
                players[i].SwitchCurrentActionMap(inGameMap.name);
            }
        }
    }

    public void Quit()
    {
        Application.Quit();
    }
}
