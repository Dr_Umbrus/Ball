using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class InGameSystem : MonoBehaviour
{
    [Header("System")]
    public int[] scores = new int[4];
    public int targetScore = 10;
    GameManager gm;
    public GameManager.ICurrentMode currentMode = GameManager.ICurrentMode.PointMatch;


    [Header("Players")]
    public Character[] characters = new Character[4];
    public GameObject[] characterObjects = new GameObject[4];

    [Header("UI")]
    [SerializeField] GameObject winner;
    [SerializeField] TextMeshProUGUI winnerText;
    [SerializeField] GameObject endButton;

    [Header("Data")]
    [SerializeField] Transform deadPlace;
    public Transform[] spawns;
    [SerializeField] ItemBox[] spawnBoxes;
    [SerializeField] GameObject baseCharacter;
    public Material[] characterColors;
    bool[] deadCharacters = new bool[4] { false, false, false, false };
    [SerializeField] GameObject deadCam;
    

    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();

        currentMode = gm.currentMode;
       

        int countPlayers = 0;
        for (int i = 0; i < gm.players.Count; i++)
        {
            if (gm.playerReady[i])
            {
                
                GameObject newChar = Instantiate(baseCharacter, deadPlace.position, Quaternion.identity);
                characters[i] = newChar.GetComponent<Character>();
                gm.players[i].GetComponent<PlayerController>().GiveCharacter(characters[i]);
                characters[i].ChangeColor(characterColors[i]);
                characterObjects[countPlayers] = newChar;
                characters[i].SetBaseBalloons();
                characters[i].aimSensitivity = gm.options.characterSpeeds[i];
                countPlayers++;
                switch (countPlayers)
                {
                    case 1:
                        if(gm.numberOfPlayers==2) { characters[i].eyes.rect = new Rect(0, .5f, 1, .5f);
                            characters[i].UIEyes.rect = new Rect(0, .5f, 1, .5f);
                        }
                        else if (gm.numberOfPlayers > 2)  {  characters[i].eyes.rect = new Rect(0, .5f, .5f, .5f);
                            characters[i].UIEyes.rect = new Rect(0, .5f, .5f, .5f);
                        }
                        else  { characters[i].eyes.rect = new Rect(0, 0f, 1f, 1f);
                            characters[i].UIEyes.rect = new Rect(0, 0f, 1f, 1f);
                        }
                        break;
                    case 2:
                        if (gm.numberOfPlayers == 2) { characters[i].eyes.rect = new Rect(0, 0f, 1, .5f);
                            characters[i].UIEyes.rect = new Rect(0, 0f, 1, .5f);
                        }
                        else { characters[i].eyes.rect = new Rect(.5f, .5f, .5f, .5f);
                            characters[i].UIEyes.rect = new Rect(.5f, .5f, .5f, .5f);
                        }
                        break;
                    case 3:
                        characters[i].eyes.rect = new Rect(0, 0f, .5f, .5f);
                        characters[i].UIEyes.rect = new Rect(0, 0f, .5f, .5f);
                        break;
                    case 4:
                        characters[i].eyes.rect = new Rect(.5f, 0f, .5f, .5f);
                        characters[i].UIEyes.rect = new Rect(.5f, 0f, .5f, .5f);
                        break;
                }
            }
        }
        if (countPlayers == 3) deadCam.SetActive(true);

        for (int i = 0; i < 4; i++)
        {
            if (characterObjects[i] != null)
            {
                characterObjects[i].transform.position = spawns[i].position;
                characters[i].ChangeState(0);
            }
        }

        switch (currentMode)
        {
            case GameManager.ICurrentMode.PointMatch:
                targetScore = gm.targetScorePoint;
                break;
            case GameManager.ICurrentMode.DeathMatch:
                targetScore = gm.targetScoreDeath;
                break;
            case GameManager.ICurrentMode.LastAlive:
                for (int i = 0; i < characters.Length; i++)
                {
                    if (characters[i] != null) characters[i].GetComponent<CharacterUI>().NoScore();
                    else
                    {
                        deadCharacters[i] = true;
                    }
                }
                break;
        }
    }

    public void DeadCharacter(int id)
    {
        

        if(currentMode== GameManager.ICurrentMode.LastAlive)
        {
            deadCharacters[id] = true;
            int countCharacters=0;
            int lastAlive = -1;
            for (int i = 0; i < deadCharacters.Length; i++)
            {
                if(deadCharacters[i] == true)
                {
                    countCharacters++;
                }
                else
                {
                    lastAlive = i;
                }
            }

            if (countCharacters == 3)
            {
                Win(lastAlive);
            }
        }
        else
        {
            characterObjects[id].transform.position = deadPlace.position;
        }
    }

    public void SpawnCharacter(int id)
    {
        characterObjects[id].transform.position = spawns[id].position;
        characters[id].ChangeState(0);
        spawnBoxes[id].ItemAvailable();
    }

    public void AddPoints(int id, int bonusScore)
    {
        scores[id] += bonusScore;
        characters[id].GetComponent<CharacterUI>().UpdateScore(scores[id]);
        if (scores[id] >= targetScore)
        {
            Win(id);
        }
    }

    public void Win(int id)
    {
        GetComponent<AudioSource>().Play();
        winner.SetActive(true);
        winnerText.text = "Player " + (id+1) + " Wins !";
        gm.SwitchMode(true);
        for (int i = 0; i < characters.Length; i++)
        {
            if (characters[i] != null)
            {
                characters[i].ChangeState(2);
            }
        }
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(endButton);
    }

    public void BackToMenu()
    {
        gm.ChangeScene(0);
    }
}
