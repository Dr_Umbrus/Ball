using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuUISystem : MonoBehaviour
{
    public enum UIModes
    {
        MainMenu=0,
        Options=1,
        PlayerSelect=2,
        LevelSelect=3,
        ShowCommands=4
    }

    UIModes uiMode = UIModes.MainMenu;
    public GameManager gm;
    public GameObject[] firsts;
    public GameObject[] menues;
    [SerializeField] AudioSource source;
    [SerializeField] AudioClip[] uiSounds;

    [Header("CharacterSelect")]
    [SerializeField] GameObject[] charactersInGame;
    [SerializeField] GameObject[] pressA;
    [SerializeField] GameObject[] pressStart;
    [SerializeField] GameObject everybodyIsReady;

    [Header("SetStats")]
    [SerializeField] TextMeshProUGUI targetScore;
    [SerializeField] GameObject scoreSelection;
    [SerializeField] TextMeshProUGUI currentMode;

    [Header("Options")]
    [SerializeField] RectTransform[] charactersExample;
    [SerializeField] TextMeshProUGUI resolution;
    [SerializeField] Slider brightSlide;
    [SerializeField] Slider soundSlide;
    [SerializeField] Slider[] playerSpeeds;
    [SerializeField] Image doubleItemImage;
    [SerializeField] Sprite noDouble, yesDouble;

    [Header("Controls")]
    [SerializeField] GameObject controlMode;
    [SerializeField] GameObject keyMode;
    [SerializeField] GameObject controlButton, keyButton;


    public static MenuUISystem instance { get; private set; }

    private void Awake()
    {
        if (instance != null) { Destroy(gameObject); }
            else instance = this;
        gm = FindObjectOfType<GameManager>();
        for (int i = 0; i < gm.players.Count; i++)
        {
            gm.players[i].GetComponent<PlayerController>().menuUI = this;
        }
        ChangeMenu(0);
    }

    public void ChangeMenu(int newMenu)
    {
        if (uiMode == UIModes.Options)
        {
            SaveSystem.SaveSetting(gm.options);
        }

        for (int i = 0; i < menues.Length; i++)
        {
            menues[i].SetActive(false);
        }
        if (menues[newMenu] != null) menues[newMenu].SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        if (firsts[newMenu] != null) EventSystem.current.SetSelectedGameObject(firsts[newMenu]);
        uiMode = (UIModes)newMenu;

        if (uiMode == UIModes.PlayerSelect) SetPlayerSelection();

        if (newMenu == 1)
        {
            SetOptions();
        }

    }

    public void SetPlayerSelection()
    {
        for (int i = 0; i < 4; i++)
        {
            if (i < gm.players.Count)
            {
                pressStart[i].SetActive(false);
                if(!gm.playerReady[i]) pressA[i].SetActive(true);
                else charactersInGame[i].SetActive(true);
            }               
        }
        if (gm.numberOfPlayers > 1) everybodyIsReady.SetActive(true);
    }

    public void StartGame(int selectedLevel)
    {
        gm.ChangeScene(selectedLevel);
    }

    public void PressStart(int id)
    {
        if(uiMode==UIModes.PlayerSelect && gm.playerReady[id]==true && gm.numberOfPlayers > 1)
        {
            ChangeMenu(3);
        }
    }

    

    public void ChangeTargetScore(int add)
    {
        gm.ChangeTargetScore(add);
        if (gm.currentMode == GameManager.ICurrentMode.PointMatch)
        {
            targetScore.text = "Target Score : " + gm.targetScorePoint;
        }
        else if(gm.currentMode == GameManager.ICurrentMode.DeathMatch)
        {
            targetScore.text = "Target Score : " + gm.targetScoreDeath;
        }
        
    }

    public void Confirm(int id)
    {
        if (uiMode == UIModes.PlayerSelect && gm.playerReady[id] != true)
        {
            gm.playerReady[id] = true;
            gm.numberOfPlayers++;
            charactersInGame[id].SetActive(true);
            pressA[id].SetActive(false);
            source.clip = uiSounds[0];
            source.Play();
            if (gm.numberOfPlayers > 1) everybodyIsReady.SetActive(true);
        }
    }

    public void Back(int id)
    {
        switch (uiMode)
        {
            case UIModes.PlayerSelect:
                if (gm.playerReady[id] == true)
                {
                    gm.playerReady[id] = false;
                    gm.numberOfPlayers--;
                    charactersInGame[id].SetActive(false);
                    pressA[id].SetActive(true);
                    source.clip = uiSounds[1];
                    source.Play();
                    if (gm.numberOfPlayers < 2) everybodyIsReady.SetActive(false);
                }
                else if (gm.playerReady[id] == false) ChangeMenu(0);
                break;
            case UIModes.LevelSelect:
                ChangeMenu(2);
                break;
                case UIModes.Options:
                ChangeMenu(0);
                break;
            case UIModes.ShowCommands:
                ChangeMenu(0);
                break;
        }

    }

    #region options

    public void ChangeDouble()
    {
        gm.doubleItems = !gm.doubleItems;
        if (gm.doubleItems) doubleItemImage.sprite = yesDouble;
        else doubleItemImage.sprite = noDouble;
    }
    void SetOptions()
    {
        ChangeModeVisual();
        soundSlide.value = gm.options.masterLevel;
        brightSlide.value = gm.options.brightness;
        for (int i = 0; i < playerSpeeds.Length; i++)
        {
            playerSpeeds[i].value = gm.options.characterSpeeds[i]/10;
        }
        resolution.text = Screen.currentResolution.width.ToString() + " x " + Screen.currentResolution.height.ToString();
    }
    public void SetMasterVolume(float sliderValue)
    {
        gm.SetMasterVolume(sliderValue);
    }

    public void SetUIVolume(float sliderValue)
    {
        gm.SetUIVolume(sliderValue);
    }

    public void SetInfoVolume(float sliderValue)
    {
        gm.SetInfoVolume(sliderValue);
    }

    public void SetGameVolume(float sliderValue)
    {
        gm.SetGameVolume(sliderValue);
    }

    public void ChangeReso(int add)
    {
        gm.ChangeResolution(gm.options.currentResolution+add);
        resolution.text = gm.options.resolutions[gm.options.currentResolution].width.ToString() + " x " + gm.options.resolutions[gm.options.currentResolution].height.ToString();
        
    }

    public void ChangeBrightness(float newBright)
    {
        gm.ChangeBrightness(newBright);
    }

    public void ChangeP1Speed(float newSpeed)
    {
        gm.ChangeCharacterSpeed(0, (int)newSpeed);
    }

    public void ChangeP2Speed(float newSpeed)
    {
        gm.ChangeCharacterSpeed(1, (int)newSpeed);
    }

    public void ChangeP3Speed(float newSpeed)
    {
        gm.ChangeCharacterSpeed(2, (int)newSpeed);
    }

    public void ChangeP4Speed(float newSpeed)
    {
        gm.ChangeCharacterSpeed(3, (int)newSpeed);
    }

    public void ChangeMode(int add)
    {
        gm.ChangeMode((int)gm.currentMode+add);
        ChangeModeVisual();
    }

    void ChangeModeVisual()
    {
        switch (gm.currentMode)
        {
            case GameManager.ICurrentMode.PointMatch:
                targetScore.text = "Target Score : " + gm.targetScorePoint;
                currentMode.text = "Point Match";
                scoreSelection.SetActive(true);
                break;
            case GameManager.ICurrentMode.DeathMatch:
                targetScore.text = "Target Score : " + gm.targetScoreDeath;
                currentMode.text = "Death Match";
                scoreSelection.SetActive(true);
                break;
            case GameManager.ICurrentMode.LastAlive:
                currentMode.text = "Last Alive";
                scoreSelection.SetActive(false);
                break;
        }
    }

    private void Update()
    {
        for (int i = 0; i < charactersExample.Length; i++)
        {
            charactersExample[i].Rotate(new Vector3(0, 0, gm.options.characterSpeeds[i]) * Time.deltaTime);
        }
    }
    #endregion

    #region controls
    public void BecomeController()
    {
        keyMode.SetActive(false);
        controlMode.SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(keyButton);
    }

    public void BecomeKeyboard()
    {
        controlMode.SetActive(false);
        keyMode.SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(controlButton);
    }
    #endregion

    public void Quit()
    {
        gm.Quit();
    }
}
