using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detonator : Item
{
    public override void Launch(int charID, Vector3 direction, Character owner)
    {
        DelayedExplosive[] explosives = FindObjectsOfType<DelayedExplosive>();
        foreach(DelayedExplosive explo in explosives)
        {
            if (explo.charID == charID) explo.Detonate();
        }
        owner.ConsumeItem();
        parentPool.AddContent(id, gameObject);
    }
}
