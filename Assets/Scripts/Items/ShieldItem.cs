using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldItem : Item
{
    private void Awake()
    {
        id = 0;
        pool = FindObjectOfType<LaunchedItemPool>();
    }

    public override void Launch(int charID, Vector3 direction, Character owner)
    {
        pool.AskContent(id, charID, direction, transform.position);
    }
}
