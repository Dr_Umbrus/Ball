using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Scriptables/Items", fileName = "ItemStats", order = 0)]
public class ItemStats : ScriptableObject
{
    public List<ItemStates> items;
}

[System.Serializable]
public class ItemStates
{
    public bool unlocked = false;
    public bool active = false;
    public int rarity;
    public bool unlockable = true;
    public GameObject itemPrefab;
    public GameObject launchPrefab;
    public Sprite storedVisual;
}
