using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBox : MonoBehaviour
{
    [SerializeField] ItemStats stats;
    [SerializeField] ItemPool pool;
    [SerializeField] List<int> activeItems= new List<int>();

    [SerializeField] LayerMask whatIsPlayer;
    [SerializeField] float detectionRadius = 1;

    [SerializeField] GameObject boxPart;
    [SerializeField] CoolDown itemCoolDown;
    [SerializeField] int minRarity=0;
    [SerializeField] bool doubleItems = false;

    private void Awake()
    {
        if (!boxPart.activeSelf) { itemCoolDown.StartCooldown(); }
        for (int i = 0; i < stats.items.Count; i++)
        {
            if(stats.items[i].active && stats.items[i].unlocked && stats.items[i].rarity>=minRarity)
            {
                for (int j = 0; j < 3-stats.items[i].rarity; j++)
                {
                    activeItems.Add(i);
                }              
            }
        }
        doubleItems = FindObjectOfType<GameManager>().doubleItems;
        pool = FindObjectOfType<ItemPool>();
    }

    private void Update()
    {
        if (boxPart.activeSelf)
        {
            Collider[] hit = Physics.OverlapSphere(boxPart.transform.position, detectionRadius, whatIsPlayer);

            if (hit.Length > 0)
            {
                for (int i = 0; i < hit.Length; i++)
                {
                    Character character = hit[i].GetComponent<Character>();
                    if (doubleItems)
                    {
                        if (character.currentItemID == -1 || character.secondaryItemID == -1)
                        {
                            int rand = Random.Range(0, activeItems.Count);
                            int result = activeItems[rand];
                            if(character.currentItemID == -1)
                            {
                                character.currentItemID = result;
                                character.GetItem(result);
                                GetComponent<AudioSource>().Play();
                            }
                            
                            else
                            {
                                character.SetSecondaryItem(result);
                                GetComponent<AudioSource>().Play();
                            }

                            itemCoolDown.StartCooldown();
                            boxPart.SetActive(false);
                            break;
                        }
                    }
                    else
                    {
                        if (character.currentItemID == -1)
                        {
                            int rand = Random.Range(0, activeItems.Count);
                            int result = activeItems[rand];
                            character.currentItemID = result;
                            character.GetItem(result);
                            GetComponent<AudioSource>().Play();

                            itemCoolDown.StartCooldown();
                            boxPart.SetActive(false);
                            break;
                        }
                    }
                    
                }               
            }
        }
        else if (itemCoolDown.coolDownComplete)
        {
            ItemAvailable();
        }
    }

    public void ItemAvailable()
    {
        boxPart.SetActive(true);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(boxPart.transform.position, detectionRadius);
    }
}
