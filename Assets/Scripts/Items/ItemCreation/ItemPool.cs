using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPool : MonoBehaviour
{
    public List<Queue<GameObject>> pools= new List<Queue<GameObject>>();
    public ItemStats stats;

    private void Awake()
    {
        for (int i = 0; i < stats.items.Count; i++)
        {
            pools.Add(new Queue<GameObject>());
        }
    }
    public void CreateContent(int id)
    {
        GameObject temp = Instantiate(stats.items[id].itemPrefab, transform.position, Quaternion.identity);
        pools[id].Enqueue(temp);
        temp.GetComponent<Item>().Awaken(id);
        temp.transform.parent = transform;
        temp.SetActive(false);
    }

    public void AskContent(int id, Character character, Transform newParent)
    {
        if (pools[id].Count <= 0)
        {
            CreateContent(id);
        }
        GameObject temp = pools[id].Dequeue();
        character.currentItem = temp.GetComponent<Item>();
        character.currentItemID = id;
        temp.SetActive(true);
        temp.transform.parent = newParent;
        temp.transform.localPosition = Vector3.zero;
        temp.transform.localRotation = Quaternion.identity;
    }

    public void AddContent(int id, GameObject objectToEnqueue)
    {
        pools[id].Enqueue(objectToEnqueue);
        objectToEnqueue.transform.parent = transform;
        objectToEnqueue.SetActive(false);
    }
}
