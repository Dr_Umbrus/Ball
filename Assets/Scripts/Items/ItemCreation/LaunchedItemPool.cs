using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchedItemPool : MonoBehaviour
{
    public List<Queue<GameObject>> pools = new List<Queue<GameObject>>();
    public ItemStats stats;

    private void Awake()
    {
        for (int i = 0; i < stats.items.Count; i++)
        {
            pools.Add(new Queue<GameObject>());
        }
    }
    public void CreateContent(int id)
    {
        GameObject temp = Instantiate(stats.items[id].launchPrefab, transform.position, Quaternion.identity);
        pools[id].Enqueue(temp);
        temp.transform.parent = transform;
        temp.GetComponent<LaunchedItem>().Awaken(id);
        temp.SetActive(false);
    }

    public void AskContent(int id, int characterID, Vector3 shotDir, Vector3 launchPoint)
    {
        if (pools[id].Count <= 0)
        {
            CreateContent(id);
        }
        GameObject temp = pools[id].Dequeue();
        
        temp.SetActive(true);
        temp.transform.position = launchPoint;
        temp.GetComponent<LaunchedItem>().SetStats(characterID, shotDir);
    }

    public void AddContent(int id, GameObject objectToEnqueue)
    {
        pools[id].Enqueue(objectToEnqueue);
        objectToEnqueue.SetActive(false);
    }
}
