using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AoE : LaunchedItem
{
    List<Character> victims = new List<Character>();

    
    public override void SetStats(int newID, Vector3 direction)
    {
        victims.Clear();
        base.SetStats(newID, direction);
    }

    protected override void Comportment()
    {

        Collider[] hit = Physics.OverlapSphere(transform.position, detectionRadius, whatIsPlayer);

        for (int i = 0; i < hit.Length; i++)
        {
            Character victim = hit[i].GetComponent<Character>();
            if (victim.characterID == charID)
            {
                victim = null;
            }
            else
            {
                for (int j = 0; j < victims.Count; j++)
                {
                    if (victim = victims[j])
                    {
                        victim = null;
                        break;
                    }
                }
            }

            if (victim != null)
            {
                Vector3 dir = victim.transform.position - transform.position;
                dir.Normalize();
                if (!RayCastCheck(transform.position, dir, whatIsWall, Vector3.Distance(victim.transform.position, transform.position)))
                {
                    victims.Add(victim);
                    HitCharacter(victim);
                }
                
            }
        }

        if (timeToLive.coolDownComplete) Disappear();
    }

    public override void Disappear()
    {
        base.Disappear();
    }
}
