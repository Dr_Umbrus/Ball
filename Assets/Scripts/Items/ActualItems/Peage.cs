using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Peage : LaunchedItem
{
    [SerializeField] List<Character> victims= new List<Character>();
    List<CoolDown> victimCD = new List<CoolDown>();
    [SerializeField] float baseCD=1;
    [SerializeField] Vector3 detectionArea;
    [SerializeField] Transform detectionVisual;
    int usesLeft = 3;

    bool activated = false;
    [SerializeField] Rigidbody rb;

    [SerializeField] AudioClip damageClip;
    [SerializeField] AudioClip baseClip;
    [SerializeField] AudioClip healClip;
    AudioSource audioSource;
    [SerializeField] CoolDown explosionDelay;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public override void SetStats(int newID, Vector3 direction)
    {
        activated = false;
        victimCD.Clear();
        victims.Clear();
        rb.isKinematic = false;
        base.SetStats(newID, direction);
        usesLeft = 3;
        detectionVisual.gameObject.SetActive(false);
    }

    protected override void Comportment()
    {
        if (activated && explosionDelay.coolDownComplete)
        {
            Collider[] hit = Physics.OverlapBox(detectionVisual.position, detectionArea, transform.rotation, whatIsPlayer);
            
            for (int i = 0; i < hit.Length; i++)
            {
                Character victim = hit[i].GetComponent<Character>();
                for (int j = 0; j < victims.Count; j++)
                {
                    if (victim = victims[j])
                    {
                        victim = null;
                        break;
                    }
                }

                if (victim != null)
                {
                    Vector3 dir = victim.transform.position - transform.position;
                    dir.Normalize();
                    if (!RayCastCheck(transform.position, dir, whatIsWall, Vector3.Distance(victim.transform.position, transform.position)))
                    {
                        victims.Add(victim);
                        victimCD.Add(new CoolDown());
                        victimCD[victimCD.Count - 1].StartCooldown(baseCD);

                        if (usesLeft > 1)
                        {
                            if (!audioSource.isPlaying)
                            {
                                audioSource.clip = damageClip;
                                audioSource.Play();
                            }
                            usesLeft--;
                            HitCharacter(victim);
                        }
                        else
                        {
                            if (!audioSource.isPlaying)
                            {
                                audioSource.clip = healClip;
                                audioSource.Play();
                            }
                            victim.Heal(victim.characterID);
                            Disappear();
                        }
                    }
                    

                    
                }
            }

            for (int i = victimCD.Count - 1; i > -1; i--)
            {
                if (victimCD[i].coolDownComplete)
                {
                    victimCD.RemoveAt(i);
                    victims.RemoveAt(i);
                }
            }
        }

        if (timeToLive.coolDownComplete)
        {
                audioSource.clip = baseClip;
                audioSource.Play();
            Disappear();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (whatIsWall == (whatIsWall | (1 << collision.gameObject.layer)) && !activated)
        {
            rb.isKinematic = true;
            activated = true;
            Vector3 target = collision.contacts[0].point + collision.contacts[0].normal * 100;
            transform.LookAt(target);
            detectionVisual.gameObject.SetActive(true);

            RaycastHit hit;

            if (Physics.Raycast(transform.position, transform.forward, out hit, 250, whatIsWall))
            {
                float wallDistance = Vector3.Distance(hit.point, transform.position);
                detectionVisual.position = (hit.point + transform.position) / 2;
                detectionArea.z = wallDistance;
                detectionVisual.localScale = new Vector3(detectionVisual.localScale.x, detectionVisual.localScale.y, wallDistance);
            }
            else
            {
                float wallDistance = 250;
                detectionVisual.position = (transform.position + transform.forward * 125);
                detectionArea.z = wallDistance;
                detectionVisual.localScale = new Vector3(detectionVisual.localScale.x, detectionVisual.localScale.y, wallDistance);
            }

            explosionDelay.StartCooldown();

        }
    }
        
}
