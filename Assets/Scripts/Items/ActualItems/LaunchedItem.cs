using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchedItem : MonoBehaviour
{
    public int charID=-1;
    protected int id;
    protected LaunchedItemPool pool;
    public int speed;
    public float detectionRadius;
    public LayerMask whatIsPlayer;
    public LayerMask whatIsWall;
    public CoolDown timeToLive;
    [SerializeField] protected bool damaging = true;
    [SerializeField] bool throwable = false;
    public bool counterable = false;
    [SerializeField] int nextPartID=-1;
    [SerializeField] protected float knockBackStr;


    public void Awaken(int newID)
    {
        id = newID;
        pool = FindObjectOfType<LaunchedItemPool>();
    }


    public virtual void SetStats(int newID, Vector3 direction)
    {
        charID = newID;
        if (throwable)
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().AddForce(direction * speed, ForceMode.Impulse);
        }
        else GetComponent<Rigidbody>().velocity = direction * speed;
        timeToLive.StartCooldown();

        Vector3 target = transform.position + direction * 100;
        transform.LookAt(target);
        GetComponent<AudioSource>().Play();
    }

    private void Update()
    {
        Comportment();
    }

    protected virtual void Comportment()
    {
        Collider[] hit = Physics.OverlapSphere(transform.position, detectionRadius, whatIsPlayer);

        if (hit.Length > 0)
        {
            Character victim = hit[0].GetComponent<Character>();
            if (victim.characterID == charID)
            {
                if (hit.Length > 1) victim = hit[1].GetComponent<Character>();
                else victim = null;
            }

            if (victim != null)
            {
                HitCharacter(victim);
                Disappear();
            }
            
        }

        hit = Physics.OverlapSphere(transform.position, detectionRadius, whatIsWall);

        if (hit.Length > 0)
        {
            Disappear();
        }

        if (timeToLive.coolDownComplete)
        {
            Disappear();
        }
    }

    protected virtual void HitCharacter(Character victim)
    {
        victim.LastDamage(charID);
        if (damaging)
        {
            victim.Damaged();
        }

        if (knockBackStr > 0)
        {
            Vector3 dir = victim.transform.position - transform.position;
            dir.Normalize();
            victim.Knockback(dir, knockBackStr);
        }
    }

    public virtual void Disappear()
    {
        if (nextPartID >= 0) Cascading(transform.position, Vector3.zero);
        pool.AddContent(id, gameObject);
    }

    public virtual void Cascading(Vector3 pos, Vector3 dir)
    {
        pool.AskContent(nextPartID, charID, dir, pos);
    }

    public bool RayCastCheck(Vector3 origin, Vector3 direction, LayerMask targets, float length)
    {
        RaycastHit hit;
        return (Physics.Raycast(origin, direction, out hit, length, targets));

        
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, detectionRadius);
    }
}
