using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MeleeItem
{
    protected override void Comportment()
    {

        Collider[] hit = Physics.OverlapBox(targetPos.position, detectionArea, transform.rotation, whatIsPlayer);

        for (int i = 0; i < hit.Length; i++)
        {
            Character victim = hit[i].GetComponent<Character>();
            if (victim.characterID == charID)
            {
                victim = null;
            }
            else
            {
                for (int j = 0; j < victims.Count; j++)
                {
                    if (victim = victims[j])
                    {
                        victim = null;
                        break;
                    }
                }
            }

            if (victim != null)
            {
                victims.Add(victim);
                HitCharacter(victim);
            }
        }

        hit = Physics.OverlapBox(targetPos.position, detectionArea, transform.rotation, whatIsWall);

        for (int i = 0; i < hit.Length; i++)
        {
            LaunchedItem victim = hit[i].GetComponent<LaunchedItem>();
            if (victim.charID == charID || !victim.counterable)
            {
                victim = null;
            }
            

            if (victim != null)
            {
                victim.GetComponent<Rigidbody>().velocity = transform.forward * victim.speed;
                victim.transform.LookAt(victim.transform.position + transform.forward * 100);
                victim.charID = charID;
            }
        }

        if (timeToLive.coolDownComplete) Disappear();
    }
}
