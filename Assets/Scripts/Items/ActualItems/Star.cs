using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : LaunchedItem
{
    List<Character> victims = new List<Character>();
    Character character;
    InGameSystem system;
    [SerializeField] Material starMat;
    [SerializeField] Gradient starGrad;
    CharacterUI cUI;
    float colorTimer = 0;

    private void Awake()
    {     
        system = FindObjectOfType<InGameSystem>();
    }

    public override void SetStats(int newID, Vector3 direction)
    {
        base.SetStats(newID, direction);
        timeToLive.StartCooldown();
        transform.parent = system.characters[newID].transform;
        character = transform.parent.GetComponent<Character>();
        character.ChangeSpeed(1.5f, timeToLive.cooldownAmount);
        character.ChangeCharacterState(2, timeToLive.cooldownAmount, 8);
        character.ChangeHealthState(1, timeToLive.cooldownAmount);
        transform.position = new Vector3(transform.parent.position.x, transform.parent.position.y + .65f, transform.parent.position.z);
        victims.Clear();
        character.ChangeColor(starMat);
        colorTimer = 0;
        cUI = character.GetComponent<CharacterUI>();
    }

    protected override void Comportment()
    {

        for (int k = 0; k < 2; k++)
        {
            Collider[] hit = Physics.OverlapSphere(transform.position + Vector3.up * 3 * k, detectionRadius, whatIsPlayer);

            for (int i = 0; i < hit.Length; i++)
            {
                Character victim = hit[i].GetComponent<Character>();
                if (victim.characterID == charID)
                {
                    victim = null;
                }
                else
                {
                    for (int j = 0; j < victims.Count; j++)
                    {
                        if (victim = victims[j])
                        {
                            victim = null;
                            break;
                        }
                    }
                }

                if (victim != null)
                {
                    victims.Add(victim);
                    HitCharacter(victim);
                    character.Heal(victim.characterID);
                }
            }
        }

        colorTimer += Time.deltaTime / timeToLive.cooldownAmount;
        cUI.ChangeContourColor(starGrad.Evaluate(colorTimer));


        if (timeToLive.coolDownComplete) Disappear();
    }

    public override void Disappear()
    {
        character.ChangeColor(system.characterColors[charID]);
        cUI.ChangeContourColor();
        transform.parent = pool.transform;
        base.Disappear();
    }
}
