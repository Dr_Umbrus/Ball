using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceShot : LaunchedItem
{
    [SerializeField] int startBounceNumber=4;
    [SerializeField] int bounceNumber;
    [SerializeField] CoolDown bounceCD;

    private void Awake()
    {
        bounceNumber = startBounceNumber;
    }
    protected override void Comportment()
    {
        Collider[] hit = Physics.OverlapSphere(transform.position, detectionRadius, whatIsPlayer);

        if (hit.Length > 0)
        {
            Character victim = hit[0].GetComponent<Character>();
            if (victim.characterID == charID)
            {
                if (hit.Length > 1) victim = hit[1].GetComponent<Character>();
                else victim = null;
            }

            if (victim != null)
            {
                HitCharacter(victim);
                Disappear();
            }

        }

        hit = Physics.OverlapSphere(transform.position, detectionRadius, whatIsWall);

        if (hit.Length > 0 && bounceCD.coolDownComplete)
        {
            if (bounceNumber > 0)
            {
                bounceNumber--;
                bounceCD.StartCooldown();
            }
            else Disappear();
        }
    }
}
