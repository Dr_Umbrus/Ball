using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashStab : LaunchedItem
{
    List<Character> victims = new List<Character>();
    Character character;
    InGameSystem system;
    [SerializeField] Color stabColor;
    CharacterUI cUI;

    private void Awake()
    {
        
        system = FindObjectOfType<InGameSystem>();
    }

    public override void SetStats(int newID, Vector3 direction)
    {
        base.SetStats(newID, direction);
        timeToLive.StartCooldown();
        transform.parent = system.characters[newID].transform;
        character = transform.parent.GetComponent<Character>();
        character.ChangeCharacterState(2, timeToLive.cooldownAmount, 8);
        character.ActDash(timeToLive.cooldownAmount, direction, character.dashMultiplier*1.5f);
        transform.position = new Vector3(transform.parent.position.x, transform.parent.position.y+.65f, transform.parent.position.z);
        victims.Clear();
        cUI = character.GetComponent<CharacterUI>();
        cUI.ChangeContourColor(stabColor);
    }

    protected override void Comportment()
    {

        for (int k = 0; k < 2; k++)
        {
            Collider[] hit = Physics.OverlapSphere(transform.position+Vector3.up*3*k, detectionRadius, whatIsPlayer);

            for (int i = 0; i < hit.Length; i++)
            {
                Character victim = hit[i].GetComponent<Character>();
                if (victim.characterID == charID)
                {
                    victim = null;
                }
                else
                {
                    for (int j = 0; j < victims.Count; j++)
                    {
                        if (victim = victims[j])
                        {
                            victim = null;
                            break;
                        }
                    }
                }

                if (victim != null)
                {
                    victims.Add(victim);
                    HitCharacter(victim);
                    character.Heal(victim.characterID);
                }
            }
        }


        if (timeToLive.coolDownComplete) Disappear();
    }

    public override void Disappear()
    {
        cUI.ChangeContourColor();
        base.Disappear();
    }

    
}
