using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpandingExplosion : AoE
{
    [SerializeField] Vector3 baseSize;
    [SerializeField] float maxSize;
    [SerializeField] float expandRate;

    public override void SetStats(int newID, Vector3 direction)
    {
        base.SetStats(newID, direction);
        transform.localScale = baseSize;
    }
    protected override void Comportment()
    {
        if (transform.localScale.x < maxSize) transform.localScale = transform.localScale + transform.localScale * expandRate * Time.deltaTime;
        else transform.localScale = new Vector3(maxSize, maxSize, maxSize);
        detectionRadius = transform.localScale.x/2;
        base.Comportment();
    }
}
