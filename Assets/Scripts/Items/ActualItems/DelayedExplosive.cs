using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayedExplosive : LaunchedItem
{
    bool isAttached = false;
    [SerializeField] Rigidbody rb;
    public override void SetStats(int newID, Vector3 direction)
    {
        rb.isKinematic = false;
        base.SetStats(newID, direction);
        isAttached = false;

    }
    protected override void Comportment()
    {
        if (timeToLive.coolDownComplete)
        {
            Disappear();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (whatIsWall == (whatIsWall | (1 << collision.gameObject.layer)) && !isAttached)
        {
            rb.isKinematic = true;
            isAttached = true;
            Vector3 target = collision.contacts[0].point + collision.contacts[0].normal * 100;
            transform.LookAt(target);
        }
    }

    public override void Disappear()
    {
        pool.AddContent(id, gameObject);
    }

    public void Detonate()
    {
        Cascading(transform.position, Vector3.zero);
        Disappear();
    }
}
