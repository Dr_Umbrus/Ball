using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeItem : LaunchedItem
{
    [SerializeField] protected Vector3 detectionArea;
    InGameSystem system;
    protected List<Character> victims = new List<Character>();
    [SerializeField] protected Transform targetPos;

    private void Awake()
    {
        system= FindObjectOfType<InGameSystem>();   
    }

    public override void SetStats(int newID, Vector3 direction)
    {
        transform.parent = system.characters[newID].transform;
        base.SetStats(newID, direction);
        victims.Clear();
    }

    protected override void Comportment()
    {
        
        Collider[] hit = Physics.OverlapBox(targetPos.position, detectionArea, transform.rotation, whatIsPlayer);

        for (int i = 0; i < hit.Length; i++)
        {
            Character victim = hit[i].GetComponent<Character>();
            if (victim.characterID == charID)
            {
                victim = null;
            }
            else
            {
                for (int j = 0; j < victims.Count; j++)
                {
                    if (victim = victims[j])
                    {
                        victim=null;
                        break;
                    }
                }
            }

            if (victim != null)
            {
                victims.Add(victim);
                HitCharacter(victim);
            }
        }

        if (timeToLive.coolDownComplete) Disappear();
    }

    protected override void HitCharacter(Character victim)
    {
        victim.LastDamage(charID);
        if (damaging)
        {
            victim.Damaged();
        }

        if (knockBackStr > 0)
        {
            victim.Knockback(transform.forward, knockBackStr);
        }
    }

    public override void Disappear()
    {
        transform.parent = pool.transform;
        base.Disappear();
    }

    private void OnDrawGizmos()
    {
      if(targetPos!=null) Gizmos.DrawWireCube(targetPos.position, detectionArea);
    }
}
