using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : LaunchedItem
{
    bool isAttached = false;
    [SerializeField] Rigidbody rb;
    [SerializeField] CoolDown explosionDelay;


    public override void SetStats(int newID, Vector3 direction)
    {
        rb.isKinematic = false;
        base.SetStats(newID, direction);
        isAttached = false;
        
    }
    protected override void Comportment()
    {
        if (isAttached && explosionDelay.coolDownComplete)
        {
            Collider[] hit = Physics.OverlapSphere(transform.position, detectionRadius, whatIsPlayer);

            if (hit.Length > 0)
            {
                for (int i = 0; i < hit.Length; i++)
                {
                    Character victim = hit[i].GetComponent<Character>();
                    if (victim.characterID == charID)
                    {
                        victim = null;
                    }
                    else
                    {
                        if (RayCastCheck(transform.position, victim.transform.position - transform.position, whatIsWall, Vector3.Distance(victim.transform.position, transform.position)))
                        {
                            victim = null;
                        }
                    }

                    if (victim != null)
                    {
                        if (!RayCastCheck(transform.position, victim.transform.position - transform.position, whatIsWall, Vector3.Distance(victim.transform.position, transform.position)))
                        {
                            Detonate();
                        }

                    }
                }
            }

            
        }

        if (timeToLive.coolDownComplete)
        {
            Disappear();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (whatIsWall == (whatIsWall | (1<<collision.gameObject.layer)) && !isAttached)
        {
            rb.isKinematic = true;
            isAttached = true;
            Vector3 target = collision.contacts[0].point + collision.contacts[0].normal * 100;
            transform.LookAt(target);
            explosionDelay.StartCooldown();
        }
    }

    public override void Disappear()
    {
        pool.AddContent(id, gameObject);
    }

    public void Detonate()
    {
        Cascading(transform.position, Vector3.zero);
        Disappear();
    }
}
