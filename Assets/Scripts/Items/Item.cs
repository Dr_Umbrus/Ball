using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    protected LaunchedItemPool pool;
    protected ItemPool parentPool;
    protected int id;
    [SerializeField] int nextPart=-1;

    public void Awaken(int i)
    {
        id = i;
        pool = FindObjectOfType<LaunchedItemPool>();
        parentPool = FindObjectOfType<ItemPool>();
    }

    public virtual void Launch(int charID, Vector3 direction, Character owner)
    {
        pool.AskContent(id, charID, direction, transform.position);
        if(nextPart>=0)
        {
            owner.currentItemID = nextPart;
            owner.GetItem(nextPart);
        }
            else owner.ConsumeItem();

        BackToPool();
    }

    public void BackToPool()
    {
        parentPool.AddContent(id, gameObject);
    }
}
