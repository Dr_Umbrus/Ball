using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptables/SceneList", fileName = "SceneLists", order = 1)]
public class ScriptableSceneList : ScriptableObject
{
    public string[] names;
}
