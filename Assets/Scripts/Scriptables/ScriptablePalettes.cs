using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptables/Palette", fileName = "palette", order = 0)]
public class ScriptablePalettes : ScriptableObject
{
    public Color[] colors;
}
