using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CharacterUI : MonoBehaviour
{
    [SerializeField] GameObject[] hp;
    
    [SerializeField] ItemStats itemStats;
    [SerializeField] ScriptablePalettes hpPalette;
    [SerializeField] Color[] hpColors;
    [SerializeField] Character myChar;
    InGameSystem system;

    [Header("Items")]
    [SerializeField] Image currentWeapon;
    [SerializeField] Image secondaryWeapon;
    [SerializeField] GameObject currentParent, secondaryParent;

    [Header("Score")]
    [SerializeField] TextMeshProUGUI tScore;
    [SerializeField] float minSize, maxSize, newScoreSize;
    float targetSize;
    [SerializeField] ScriptablePalettes scoreColors;
    [SerializeField] CoolDown ScoreCD;

    [Header("Death")]
    [SerializeField] TextMeshProUGUI deathTimer;
    CoolDown deathCount=new CoolDown();


    [Header("RedScreen")]
    [SerializeField] CoolDown damageCD;
    [SerializeField] Image redScreen;
    [SerializeField] Color baseRedScreen;

    [Header("DamageWheel")]
    [SerializeField] Image indicator;
    [SerializeField] Color baseIndicator;
    [SerializeField] CoolDown indicatorCD;
    [SerializeField] Transform indicatorParent;
    Transform target;
    [SerializeField] Transform player;

    [Header("Contour")]
    [SerializeField] Image contour;
    [SerializeField] Color baseContourColor;
    

    private void Awake()
    {
        system = FindObjectOfType<InGameSystem>();
    }

    public void UpdateScore(int score)
    {
        tScore.text = score.ToString();
        ScoreCD.StartCooldown();
        tScore.color = Color.Lerp(scoreColors.colors[0], scoreColors.colors[1], score);
        targetSize = system.scores[myChar.characterID] / system.targetScore;
    }


    public void NoScore()
    {
        tScore.gameObject.SetActive(false);
    }

    public void ChangeContourColor(Color color)
    {
        contour.color = color;
    }

    public void ChangeContourColor()
    {
        contour.color = baseContourColor;
    }

    public void ChangeLife(int newHP)
    {
        deathTimer.text = "";
        for (int i = 0; i < hp.Length; i++)
        {
            if (i < newHP)
            {
                if (!hp[i].activeSelf)
                {
                    hp[i].SetActive(true);
                    hp[i].GetComponent<Fade>().Fading(.5f, hpColors[i]);
                }
                else
                {
                    hp[i].GetComponent<Image>().color = hpColors[i];
                }
            }
            else
            {
                if (hp[i].activeSelf)
                {
                    
                    hp[i].GetComponent<Fade>().Fading(.5f, Color.clear);
                }
            }
        }
    }

    private void Update()
    {
        redScreen.color = new Color(baseRedScreen.r, baseRedScreen.g, baseRedScreen.b, Mathf.Lerp(baseRedScreen.a, 0, damageCD.ratio));
        indicator.color = new Color(baseIndicator.r, baseIndicator.g, baseIndicator.b, Mathf.Lerp(baseIndicator.a, 0, indicatorCD.ratio));
        if (target != null)
        {
            Vector3 direction = player.position - target.position;
            Quaternion tRot = Quaternion.LookRotation(direction);
            tRot.z = -tRot.y;
            tRot.x = 0;
            tRot.y = 0;

            Vector3 north = new Vector3(0, 0, player.eulerAngles.y);
            indicatorParent.localRotation = tRot * Quaternion.Euler(north);
        }

        if (!deathCount.coolDownComplete)
        {
            deathTimer.text = Mathf.RoundToInt(deathCount.m_cooldownComplete - Time.time).ToString();
        }

        if (!ScoreCD.coolDownComplete)
        {
            tScore.fontSize = Mathf.Lerp(newScoreSize, targetSize, ScoreCD.ratio);
        }
        else
        {
            tScore.fontSize = Mathf.Lerp(minSize, maxSize, system.scores[myChar.characterID] / system.targetScore);
        }
    }

    public void Damaged(int targetID)
    {
        damageCD.StartCooldown();
        if (targetID > 0)
        {
            indicatorCD.StartCooldown();
            target = system.characterObjects[targetID].transform;
        }
        
    }

    public void Death(float time)
    {
        deathCount.StartCooldown(time);
    }

    public void ChangeHPColor(int targetHP, int color)
    {
        hpColors[targetHP] = hpPalette.colors[color];
    }

    public void ChangeWeapon()
    {
        if (myChar.currentItemID >= 0)
        {
            if (itemStats.items[myChar.currentItemID].storedVisual != null)
            {
                currentParent.SetActive(true);
                currentWeapon.sprite = itemStats.items[myChar.currentItemID].storedVisual;
            }
        }
        else currentParent.SetActive(false);

        if (myChar.secondaryItemID >= 0)
        {
            if (itemStats.items[myChar.secondaryItemID].storedVisual != null)
            {
                secondaryParent.SetActive(true);
                secondaryWeapon.sprite = itemStats.items[myChar.secondaryItemID].storedVisual;
            }
        }
        else secondaryParent.SetActive(false);
    }
}
