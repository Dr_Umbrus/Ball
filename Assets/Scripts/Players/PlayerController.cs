using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    public Character myCharacter;
    public int playerID=-5;
    public MenuUISystem menuUI;

    private void Awake()
    {
        menuUI = FindObjectOfType<MenuUISystem>();
    }

    #region Menu
    public void OnConfirm()
    {
        menuUI.Confirm(playerID);
    }
    
    public void OnBack()
    {
        menuUI.Back(playerID);
    }

    public void OnStart()
    {
        menuUI.PressStart(playerID);
    }
    #endregion

    #region Gameplay
    public void OnMove(InputValue value)
    {
        if (myCharacter != null)
        {
            myCharacter.Move(value.Get<Vector2>());
        }
    }

    public void OnRightStick(InputValue value)
    {
        if (myCharacter != null)
        {
            myCharacter.RightStick(value.Get<Vector2>());
        }
    }

    public void OnJump()
    {
        if (myCharacter != null)
        {
            myCharacter.Jump();
        }
    }

    public void OnShoot()
    {
        if (myCharacter != null)
        {
            myCharacter.Shoot();
        }
    }

    public void OnDash()
    {
        if (myCharacter != null)
        {
            myCharacter.NormalDash();
        }
    }

    public void OnTurnaround()
    {
        if (myCharacter != null) myCharacter.TurnAround();
    }
    #endregion

    public void GiveCharacter(Character chara)
    {
        myCharacter = chara;
        chara.characterID = playerID;
    }
}
