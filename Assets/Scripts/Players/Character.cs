using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Character : MonoBehaviour
{
    [System.Serializable]
    public enum IMovementStates { Null=0, Dash=1, Waiting=2, ChangeSpeed=3 };
    enum IStates { Null=0, Ghost=1, GoThrough=2}
    enum IHealthStates { Null=0, Invincible=1, Dead=2, Ghost=3};

    

    [Header("Components")]
    public Camera eyes;
    public Camera UIEyes;
    [SerializeField] Transform cameraEyes;
    [SerializeField] Transform body;
    [SerializeField] MeshRenderer mesh;
    CharacterController cc;
    [SerializeField] CharacterUI cUI;
    [SerializeField] MeshRenderer capsule;

    [Header("Movement")]
    [SerializeField] float moveSensitivity;
    float baseMoveSensitivity;
    public float aimSensitivity;
    float xRotation = 0;
    [SerializeField] float jumpHeight = 6;
    Vector3 velocity;
    public Vector2 leftStick;
    public Vector2 rightStick;
    [SerializeField] float baseKnockBackDuration=.25f;

    [Header("Ground")]
    public Transform groundCheck;
    public float groundDistance = .4f;
    public LayerMask whatIsGround;
    bool isGrounded;

    InGameSystem system;
    [SerializeField] IMovementStates movementState=IMovementStates.Waiting;
    public int characterID=-1;
    int lastDamaged = -1;
    Material baseMat;

    [Header("Life")]
    public int life;
    [SerializeField] GameObject[] balloons;
    IHealthStates healthState=IHealthStates.Null;
    public CoolDown invulnerabilityFrames;
    [SerializeField] float baseInvulnerabilityTime, deathCount;
    


    [Header("Dash")]
    public CoolDown dashCD;
    public CoolDown dashDurationCD;
    public float baseDashDuration;
    public float dashMultiplier;
    bool canDash = true;
    Vector3 targetDash;
    [SerializeField] CoolDown turnAround;
    

    [Header("Items")]
    public Item currentItem = null;
    [SerializeField] ShieldItem basicShield;
    ItemPool pool;
    public int currentItemID = -1;
    public int secondaryItemID = -1;
    public Transform weaponSocket;
    [SerializeField] CoolDown shieldCD;

    [Header("CharacterStates")]
    IStates characterState = IStates.Null;
    [SerializeField] CoolDown timeOtherState;

    [Header("Audio")]
    [SerializeField] AudioSource damageSource;
    [SerializeField] AudioSource jumpSource;
    [SerializeField] AudioSource dashSource;

    #region introduction
    private void Awake()
    {
        cc = GetComponent<CharacterController>();
        pool = FindObjectOfType<ItemPool>();
        system = FindObjectOfType<InGameSystem>();
        baseMoveSensitivity = moveSensitivity;
        cUI.ChangeWeapon();
        shieldCD.StartCooldown();
        turnAround.StartCooldown();
    }

    public void ChangeColor(Material mat)
    {
        mesh.material = mat;
        baseMat = mat;
        
    }

    public void SetBaseBalloons()
    {
        for (int i = 0; i < 5; i++)
        {
            cUI.ChangeHPColor(i, characterID);
            ChangeBalloonColor(i, characterID);
        }
        UpdateBalloons();
    }

    public void ChangeState(int newState)
    {
        movementState = (IMovementStates)newState;
    }
    #endregion


    #region movements

    public void Move(Vector2 value)
    {
        if (movementState != IMovementStates.Dash) {
            leftStick = value;

            if (Mathf.Abs(leftStick.x) < .3f)
            {
                leftStick.x = 0;
            }
            if (Mathf.Abs(leftStick.y) < .3f)
            {
                leftStick.y = 0;
            }
        }
        
    }

    public void RightStick(Vector2 value)
    {
        if (movementState == IMovementStates.Dash) rightStick = Vector2.zero;
        else
        {
            rightStick = value;

            if (Mathf.Abs(rightStick.x) < .3f)
            {
                rightStick.x = 0;
            }
            if (Mathf.Abs(rightStick.y) < .3f)
            {
                rightStick.y = 0;
            }
        }
        
    }

    public void Jump()
    {
        if (isGrounded)
        {
            jumpSource.Play();
            velocity.y = Mathf.Sqrt(jumpHeight * -2 * Values.gravity);
        }
    }

    public void TurnAround()
    {
        if (turnAround.coolDownComplete)
        {
            body.Rotate(Vector3.up * 180);
            turnAround.StartCooldown();
        }
    }

    #region dash
    public void NormalDash()
    {
        if (movementState != IMovementStates.Waiting && canDash && characterState == IStates.Null)
        {
            dashSource.Play();
            ActDash(baseDashDuration, cameraEyes.transform.forward, dashMultiplier);
            canDash = false;
            dashCD.StartCooldown();
        }
    }

    
    public void ActDash(float duration, Vector3 direction, float strength)
    {
        movementState = IMovementStates.Dash;
        
        targetDash =  direction*strength ;
        dashDurationCD.cooldownAmount = duration;
        dashDurationCD.StartCooldown();
    }
    #endregion
#endregion

    #region items
    public void GetItem(int itemID)
    {
        pool.AskContent(itemID, this, weaponSocket);
        cUI.ChangeWeapon();
    }

    public void SetSecondaryItem(int itemID)
    {
        secondaryItemID = itemID;
        cUI.ChangeWeapon();
    }
    public void Shoot()
    {
        if (movementState == IMovementStates.Null && healthState!=IHealthStates.Ghost)
        {
            if (currentItem == null)
            {
                if (shieldCD.coolDownComplete)
                {
                    basicShield.Launch(characterID, cameraEyes.transform.forward, this);
                    shieldCD.StartCooldown();
                }
            }
            else
            {                
                currentItem.Launch(characterID, cameraEyes.transform.forward, this);
            }
        }  
    }

    public void ConsumeItem()
    {
        if (secondaryItemID >=0)
        {           
            currentItemID = secondaryItemID;
            GetItem(currentItemID);
            SetSecondaryItem(-1);
        }
        else
        {
            currentItem = null;
            currentItemID = -1;
            cUI.ChangeWeapon();
        }
    }
    #endregion

    

    private void Update()
    {
        if (movementState != IMovementStates.Waiting)
        {
            isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, whatIsGround);

            if (isGrounded && velocity.y < 0) velocity.y = -2;
            if (isGrounded && !canDash && dashCD.coolDownComplete) canDash = true;

            if (movementState == IMovementStates.Dash)
            {
                cc.Move(targetDash * moveSensitivity * Time.deltaTime);
                
            }
            else
            {
                Vector3 movement = transform.right * leftStick.x + transform.forward * leftStick.y;

                cc.Move(movement * moveSensitivity * Time.deltaTime);

                velocity.y += Values.gravity * Time.deltaTime;
                cc.Move(velocity * Time.deltaTime);
            }

            float headHorizontal = rightStick.x * aimSensitivity * Time.deltaTime;
            float headVertical = rightStick.y * aimSensitivity * Time.deltaTime;

            body.Rotate(Vector3.up * headHorizontal);
            xRotation -= headVertical;
            xRotation = Mathf.Clamp(xRotation, -90, 90);
            cameraEyes.localRotation = Quaternion.Euler(xRotation, 0, 0);
        }

        if (dashDurationCD.coolDownComplete && (movementState == IMovementStates.Dash || movementState == IMovementStates.ChangeSpeed))
        {
            movementState = IMovementStates.Null;
            moveSensitivity = baseMoveSensitivity;
        }

        if (healthState == IHealthStates.Invincible)
        {
            if (invulnerabilityFrames.coolDownComplete) healthState = IHealthStates.Null;
        }
        if (healthState == IHealthStates.Dead)
        {
            if (invulnerabilityFrames.coolDownComplete)
            {
                healthState = IHealthStates.Null;
                system.SpawnCharacter(characterID);
                life = 3;
                SetBaseBalloons();
                UpdateBalloons();
            }
        }

        if(characterState!=IStates.Null && timeOtherState.coolDownComplete)
        {
            characterState = IStates.Null;
            mesh.material =baseMat;
            gameObject.layer = 7;
        }
    }

    #region Combat
    public void LastDamage(int id)
    {
        lastDamaged = id;
    }

    public void Damaged()
    {
        if (healthState == IHealthStates.Null)
        {
            damageSource.Play();
            life--;
            if (life > 0)
            {
                if(system.currentMode == GameManager.ICurrentMode.PointMatch && lastDamaged != characterID && lastDamaged>=0)
                {
                    system.AddPoints(lastDamaged,1);
                }
                ChangeHealthState(1, baseInvulnerabilityTime);
                cUI.Damaged(lastDamaged);
                StartCoroutine(InvincibiliBlink());
                UpdateBalloons();
            }
            else
            {
                if (system.currentMode == GameManager.ICurrentMode.PointMatch && lastDamaged != characterID && lastDamaged >= 0)
                {
                    system.AddPoints(lastDamaged, 2);
                }
                else if(system.currentMode == GameManager.ICurrentMode.DeathMatch)
                {
                    system.AddPoints(lastDamaged, 1);
                }
                UpdateBalloons();
                Death();
            }
            
        }
    }

    public void Heal(int sourceID)
    {
        if (life < 5)
        {
            life++;
            cUI.ChangeHPColor(life-1, sourceID);
            ChangeBalloonColor(life - 1, sourceID);
        }
        UpdateBalloons();
    }

    public void ChangeBalloonColor(int balloonID, int material)
    {
        balloons[balloonID].GetComponent<MeshRenderer>().material = system.characterColors[material];
    }

    void UpdateBalloons()
    {
        for (int i = 0; i < 5; i++)
        {
            if (i < life) balloons[i].SetActive(true);
            else balloons[i].SetActive(false);
        }
        cUI.ChangeLife(life);
    }

    public void Death()
    {
        if (currentItemID >= 0)
        {
            currentItem.BackToPool();
            currentItemID = -1;
        }
        movementState = IMovementStates.Waiting;
        if (system.currentMode == GameManager.ICurrentMode.LastAlive)
        {
            system.DeadCharacter(characterID);
            characterState = IStates.Ghost;
            healthState = IHealthStates.Ghost;
            gameObject.layer = 10;
            capsule.enabled = false;
        }
        else
        {
            cUI.Death(deathCount);
            ChangeHealthState(2, deathCount);
            system.DeadCharacter(characterID);
        }
    }

    public void Knockback(Vector3 direction, float strength)
    {
        ActDash(baseKnockBackDuration, direction, strength);
    }

    IEnumerator InvincibiliBlink()
    {
        while (healthState == IHealthStates.Invincible)
        {
            mesh.enabled = false;
            yield return new WaitForSeconds(.1f);
            mesh.enabled = true;
            yield return new WaitForSeconds(.1f);
        }
    }
    #endregion

    public void ChangeCharacterState(int newState, float duration, int newLayer)
    {
        timeOtherState.cooldownAmount = duration;
        timeOtherState.StartCooldown();
        gameObject.layer = newLayer;
        characterState = (IStates)newState;
    }

    public void ChangeHealthState(int newState, float duration)
    {
        healthState = (IHealthStates)newState;
        invulnerabilityFrames.cooldownAmount = duration;
        invulnerabilityFrames.StartCooldown();
    }

    public void ChangeSpeed(float speedMultiplier, float duration)
    {
        movementState = IMovementStates.ChangeSpeed;
        dashDurationCD.cooldownAmount = duration;
        dashDurationCD.StartCooldown();
        moveSensitivity *= speedMultiplier;
    }
}
