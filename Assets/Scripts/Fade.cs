using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fade : MonoBehaviour
{
    Color currentCol;
    Color targetCol=Color.red;
    CoolDown fadeCD= new CoolDown();
    Image image;

    private void Awake()
    {
        image = GetComponent<Image>();
    }
    public void Fading(float time, Color target)
    {
        currentCol = image.color;
        targetCol = target;
        fadeCD.StartCooldown(time);
    }

    private void Update()
    {
        if (!fadeCD.coolDownComplete)
        {
            image.color = Color.Lerp(currentCol, targetCol, fadeCD.ratio);
        }
        else if (targetCol == Color.clear)
        {
            gameObject.SetActive(false);
        }
    }
}
