using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CoolDown
{
    public float cooldownAmount;
    public float m_cooldownComplete;
    float startTime;

    public float ratio => (Time.time-startTime)/(m_cooldownComplete-startTime);
    public bool coolDownComplete => Time.time > m_cooldownComplete;

    public void StartCooldown()
    {
        startTime = Time.time;
        m_cooldownComplete = Time.time + cooldownAmount;
    }

    public void StartCooldown(float amount)
    {
        cooldownAmount = amount;
        startTime = Time.time;
        m_cooldownComplete = Time.time + cooldownAmount;
    }
}
