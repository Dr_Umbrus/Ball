using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlane : MonoBehaviour
{
    InGameSystem system;
    private void Awake()
    {
        system = FindObjectOfType<InGameSystem>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Character cha = other.GetComponent<Character>();
        if (cha.life > 0)
        {
            cha.Damaged();
        }       
        other.transform.position = system.spawns[cha.characterID].position;
    }
}
